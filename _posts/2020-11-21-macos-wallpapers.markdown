---
layout: post
title:  "macOS wallpapers"
categories: 
    - tinkering
---
when i started to use a computer with Mac OS X 10.2 "Jaguar", my main focus was to gather loads and loads of desktop pictures that meant something to me, or that were awe inspiring.

with every major upgrade of my system, new colorful and fascinating pictures were added to my collection.

lately, i've been delving into the nostalgia of re-exploring the wallpapers of my childhood, the ones that came with every new version of Mac OS X.

fueled by a similar endeavor by [512pixels](https://512pixels.net/projects/default-mac-wallpapers-in-5k/), who upscaled some pictures to 5K resolution.

while I really liked the idea, i'm not a fan of upscaling and prefer proper archiving.

i began to gather every Mac OS X installer, from 10.0 Cheetah to 11 Big Sur, and extract the default desktop pictures folder, which you can download here :

* [Complete Set (with duplicates)](https://mega.nz/file/oclznIjR#WmNnIw2NEM6TLkp0OaHwtSAflmhc81tL39RBzWFnj9o)
* [10.0 Cheetah](https://mega.nz/file/gA1D3CBS#MVmMNbXtEuznjRRHCo_OFSqXquZbfzLiGriXlWJ3A3s)
* [10.0 Cheetah Server](https://mega.nz/file/pQ8lwIyI#9ocuP1q_9joqg9xSsQdWbUhrtguADwRbeCOE8KYS0l0)
* [10.1 Puma](https://mega.nz/file/wd8HDQTJ#C7XbOoNSrW9yLkzetHsutp2YnhWR9boWA3WNEb-aWAc)
* [10.1 Puma Server](https://mega.nz/file/5cl3RIbD#klcGnl35b7Tw9MO01n2Un8__TbK3it7J-OJCjvZROaU)
* [10.2 Jaguar](https://mega.nz/file/oBsVFIzZ#Qm7XCthO7QpQJXc-WIhcMNg1CO5K6oIQZYXDz8rKkSQ)
* [10.2 Jaguar Server](https://mega.nz/file/RItVkAYa#puMftiSnojNp8CqYtJPdprudu2OPe2LKnRALMnLZeu4)
* [10.3 Panther](https://mega.nz/file/EFsn2AqK#WJ6z0E_7ZesOh_v7qY7u-XyTBWQJqTbwuaW8lPWOZ1E)
* [10.3 Panther Server](https://mega.nz/file/QNt1QazY#8k2ie2lcD6YpgQNgJIBji6bNoNGZXxoEQjUz_4b_c5w)
* [10.4 Tiger](https://mega.nz/file/RQk1TADR#fPuRZAyw3Ei4_adBTSEptAcNpHjgv-sJkwXLj1Hx6FY)
* [10.4 Tiger Server](https://mega.nz/file/oclzEYzJ#BaaV2Hw7sJOpQl2XJRqERyLTVvQUVA_heuM5lUewl-0)
* [10.5 Leopard](https://mega.nz/file/sRkDFIbQ#adCTZXEPSfdygI7hUpvE2VdOVIxvD-EWONspdaZjTRg)
* [10.5 Leopard Server](https://mega.nz/file/UZ13BIgL#snfcrI_3ncwzJcL25I4iJwKoBriR9wrIMwv9q_Ic8Gs)
* [10.6 Snow Leopard](https://mega.nz/file/NY0HkIKL#2rbsdo1T9NOmRPb_427fRBh5eicxZgdCQDEpyr48eqM)
* [10.6 Snow Leopard Server](https://mega.nz/file/IU91VYBA#20UVxyTbzTA0ShwQJVznYc3qFTP9Bt9zU9wedPxLaqM)
* [10.7 Lion](https://mega.nz/file/4N1jXYSb#-r2hHKE8AC_W0eoRw3T8SwhSSancpTeVGZnwzMhQ69M)
* [10.8 Mountain Lion](https://mega.nz/file/YZ0xVQyJ#ivJI9pWz-J9AZRuDs4GVTgMA_aKbHoIc9g59tWDT9ws)
* [10.9 Mavericks](https://mega.nz/file/ZUkXiIZY#x47NOu7nSL_s3BLfI4LjL7p_ZpA3YmKDTOEX4cgUdgY)
* [10.10 Yosemite](https://mega.nz/file/8ckHBQDa#ETjwoel7FQMMvLNM0UyewZ_zPqO9ryLqyaAgNaKUaGA)
* [10.11 El Capitan](https://mega.nz/file/0I9D2S5Y#iK9Rsdti_-JJ83xosaQHa3fAx3aAn48yjArecG0GDgc)
* [10.12 Sierra](https://mega.nz/file/MV1BiYxC#4N6c1jL6AtBp_LOQajbxGWPdEV5VzKaJwv6nPEsbpkc)
* [10.13 High Sierra](https://mega.nz/file/5Zs12CZR#zjp_7NJgajiUVx4pKF9WaxUDzebNXfkyp6sdOjs0CDo)
* [10.14 Mojave](https://mega.nz/file/oQ0B0ART#0DcQpUPkQoValZWbzafqQmYoB39ABB1A7jxBqGr8zCQ)
* [10.15 Catalina](https://mega.nz/file/xZsBBYKb#LTvyROvqbREwelR4HldYifci4aKbcS6fixRR40W4C_A)
* [11.0 Big Sur](https://mega.nz/file/NR8FRKCZ#WZBJcQhXbNnMtS5T14eJr6HB-lprDw69Xjm7hPriZLE)
* [Promo Shots](https://mega.nz/file/wV9z3IpB#y6JSmhASxw0jGUBjny8mf2-HVGBSKnU7eLW3f4Tjthk)

*- since 10.7 Lion, there is no more a separate "server" installer, only an app that gives the same functionalities.*
	
then, i compiled every images (and removed duplicates) to create a giant folder of every pictures ever used as a wallpaper by a default installation of Mac OS X : [Download the complete combined set](https://mega.nz/file/dI9RiKKK#159Qt0y7A-PkSs0OipXpI8d9kLU6js_lp4XcqRooBdk)

*- the Promo Shots were omitted*

