---
layout: post
title:  "myths on electric cars"
categories: 
    - beyondinnovations
    - tinkering
---
When we explore the idea of an electric car and what it implies (mostly breaking ties with the old way of doing things), we quickly stumble upon a vast group of people who devote their lives to discredit the whole concept, by doubts and fear.

Here's the rough ideas with which they'll try to convince you that electric cars are a conspiracy created by a [pot smoking alien](https://www.theverge.com/2018/9/7/17830810/elon-musk-smokes-weed-electric-plane-design-joe-rogan-podcast).

## electric cars are polluting as much as an ICEV
In some States/Countries, electricity is made from big power stations, powered by an oil, gas or nuclear fuel. In those places, charging an electric car seems to only divert the fuel consumption to the power plants. This is mostly correct, except that using an electric car instead of an ICEV will still result in a much lower carbon footprint (or tiretrack...) and a much lower fuel consumption. 

Here's the catch, because the combustion engine needs to fit inside an engine bay, be light enough as to not affect the car too much and be quiet enough not to disturb the car's occupants, it needs to compromise on something else. A car combustion engine will compromise on efficiency. A modern one will probably get up to a measly 25% efficiency, meaning that for every drop of fuel, [only 25% of it is converted into actual *work*](https://en.wikipedia.org/wiki/Engine_efficiency#Internal_combustion_engines). Even worst, when idling, the efficiency drop close to 0%, since no *work* is being done (except heating and a small electrical charge).

On the other hand, power stations don't have those kinds of limitations, they can be as big, as heavy and run as long as they want. In fact, their only limitation is to be as efficient as possible because their profitability depends on it. Modern thermal power station have an efficiency of [about 60%](https://www.ge.com/power/about/insights/articles/2016/04/power-plant-efficiency-record). This makes power stations exactly 240% more efficient than combustion engine in cars. Let's see how that translate into real-world usage : If you can travel 100 km with 10 L of petrol (10 L / 100 km) with an ICEV, the same 10L of petrol in a power plant could generate enough electricity to drive 240 km.

Of course this calculation doesn't take into account power transmission losses, battery efficiency, and the monetary & energy cost used when refining and transporting petrol to the petrol stations. Even if we factor it in, the trend is clear : It is at least 2 times more efficient (and cleaner) to drive an electric car instead of an ICEV, even when the electricity you use to charge it comes from a *dirty* source. Imagine when your electricity comes from Solar, Wind or Hydro!


## batteries are not recyclable and are a big pollution source
Because of all the weird-sounding names like Lithium and Ion inside the battery, people think that it's highly pollution (during its production or afterlife) and that we can't even recycle it.

In reality, It is true that [producing a car battery](https://www.wired.com/2016/03/teslas-electric-cars-might-not-green-think/) involves lots of polluting processes : Extracting Lithium, Nickel and Cobalt are very hazardous and can contaminate water wells, among others. On the other side, batteries can be recycled, its chemistry can be restored or its cells can be repurposed for stationary use (like [Nissan](http://fortune.com/2015/06/15/electric-car-batteries-reuse/) does).

## charging could overload the grid
Let's consider Canada's case : we have around [33 million cars](https://www150.statcan.gc.ca/n1/daily-quotidien/170629/dq170629d-eng.htm) on the road, with an average 20 000 km per year. Knowing that an electric car consumes about 20 kWh per 100 km, if every car in Canada was an electric one, the total needed electricity would amount to 130 million kWh yearly ( 3600 kWh per capita ). Adding this to the 15.5 MWh [total electricity consumption per capita](https://www.nrcan.gc.ca/energy/facts/electricity/20068) will bring it to 19.1 MWh yearly per capita. Changing all cars in Canada to electric would add only 23% of the total electricity consumption. Furthermore, because most of the charging occurs on off-peak hours, we could use up the surplus we currently have, and we would not need to add as much infrastructure.

Now the killer argument is this : extracting and refining crude oil into petrol takes a lot of energy : [more than 5 kWh per litre of fuel](https://www.businessinsider.com/elon-musk-and-chris-paine-explain-how-the-electric-car-got-its-revenge-2011-10). For and ICEV to travel 100 km (at 10 L / 100 km), it will have used up more than 50 kWh, whereas an electric car will use around 20 kWh. Another 2.5x more efficient. Knowing this, if every can in Canada was electric, it would actually *LOWER* our total electricity consumption (because we extract and refine a lot of petrol). 

## anxiety
Detractors believe that electric cars are not really usable because of the long time it takes to charge them and the small range they provide.

While it's true that charging on a Level1 charger can take a very long time (8hrs+), most of the charging happens on Level2 or Level3, with a charging time of 5hrs or 30-45mins. Next, consider that for most of the day, the car is parked and unused. Be it at your house, work, parking lot, etc. We can easily charge the car whenever it's not in use, even if we are at 80% charge, because plugin the car takes only a few seconds, compared to filling up a ICEV tank.

Range anxiety depends greatly on the person's way of life : most of Canadians travel less than 50 km per day, meaning that they need to charge once in 10 days for a Tesla Model 3, and 4 days for a Nissan Leaf. Not bad at all! Do not forget that we do not need to go to special charging stations, because electricity is everywhere.

## final word
While it's true that when both cars go out of the production line, the electric car if *dirtier* because of all the rare metals inside the battery and motor. But, as soon as the car travels 30 000 km, the total emissions are equal. After that, [the electric car is greener than the ICEV one](http://www.hydroquebec.com/data/developpement-durable/pdf/analyse-comparaison-vehicule-electrique-vehicule-conventionnel.pdf).