---
layout: post
title:  "archive: Asteroid Labs"
categories: 
    - archives
---

Following Bitcoin and Cryptocurrencies since 2013, when I helped to kickstart the Bitcoin Embassy in Montréal, I am now building a revolutionary project in this field. Started from the lack of adequate tools and scatered services, we are porting Financial tools to cryptos, porting multiple projects under one banner with single login and drasticaly reducing fees. Read more on [asteroidlabs.io](https://asteroidlabs.io).

See also : [*Meteor Trader : automatic trading using a custom-baked indicator*](https://couteausuis.se/asteroidlabs/2018/10/13/potency/)