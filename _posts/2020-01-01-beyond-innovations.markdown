---
layout: post
title:  "archive: Beyond Innovations"
categories: 
    - archives
---

Having a big sensitivity to climate change, sustainable developments and renewable energy, a friend and I started a to build recycled and recyclable domestic-sized wind turbines. We are currently building weather stations to gather data and help finance the project. Subscribe to stay tuned, at [beyondinnovations.tech](https://beyondinnovations.tech).