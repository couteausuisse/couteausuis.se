---
layout: post
title: "a note about notes"
categories: 
    - tinkering
---
## intro
even since i started using computers, my organisation has been a mess. i'm lucky enough that my memory is good and can compensate my lack of organisation, but as i'm getting older, i'm trying to be mindfull of leaving documentation traces, notes, etc. thus began my search for the perfect notes organisation tool.

## requirements
to map my brain model, the tool should have everything i will ever need for the rest of my life, a one-stop-shop of notes and other contents. remove silos and lower friction like copy-pasting in multiple apps, remembering different keybindings, quirks, different design trends.

conceptualy, i'm more used to a hierarchical (or top-down) approach, but i really like the network (or bottom-up) approach. i guess the ideal is in the middle, where i would organise hierarchicaly (by projects or types) while having an automatic keyword network.

feature-wise, the tool should offer no friction while being used simultaneously as :

- quick notes or scratch pad
- todos
- daily stoic journal
- organized notes
- project management
- publishable knowledge base (Notion)
- programming notebook (jupyter)

blocks or notes should be freely moveable between usages, ex. a quick note can be a moved into a project task, or into a notebook, etc.

## continuing the thought process
if we push a bit more the reflexion:

- most emails i receive leads to tasks or notes to take -> they want X information, i received Y information regarding a project.
- calendar events are mostly results from tasks or things to not forget -> meeting regarding project Z (agenda includes notes A,B,C and results in notes ZA, ZB, ZC). 
- "take a dentist appointment" is a calendar event that would be best suited as a note in the daily journal.
- while programming, i often need to test snippets of code, or document it for a knowledge base.
- code bugs often leads to tasks in a project.
- code and research time if often billed or noted in the daily journal.
- while managing a project, i often have to deal with other types of files -> those files could be linked to notes and stored appropriately.
- paper files needs organizing too and often leads to tasks or notes.

thus, it would make sense to replace the function of a file browser, calendar and mailbox, or imbed them in a certain way (to not reinvent the wheel).

It would also make sense to be able to execute python code and sql queries directly in the notebook (via linked files and snippets), with code versioning and project scaffolding.

## other considerations
the tool should abstract away the filesystem, be it local, remote, cloud, services or a multiple of those while not moving files or blocking other apps from using them (ex. VSCode for bigger projects). it should easily allow exportations and migrations.

for exemple, one should be able to link a paperless-ngx instance to catalog and use files stored there in various notes.

the tool should be pretty but unobstrutive and allow customization, via themes and plug-in, offer vim/emacs/native keybindings and a web browser extension.

most importantly, the tool should offer native clients (macOS/iOS), with a self-hosted open-source server.

## contenders
in my lengthly search over the years, i tried multiple apps that seek to acheive some of those requirements:

- Notion (closed source and not self-hosted)
- Outline (geared only towards KB/team)
- Logseq (too much a network of notes)
- Standard Notes (still too much silo)
- AnyType (not self-hosted or customizable)
- BookStack (geared only towards a wiki)
- Joplin (too much hierarchical)

but with limited time on this earth, i cannot settle for an incomplete tool to organize my whole life.

## next steps
i know most of the requirements could be acheived with emacs and org-mode (even email and calendar). 

i've used those tools before and unfortunately, the commands/actions take too much brain-space for me. i need the tool to be graphic first, with key combos that i can learn at my own pace.

stay tuned for Citrus Notebook...