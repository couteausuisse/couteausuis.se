---
layout: post
title:  "email non-revolution"
categories: 
    - tinkering
---

When we look back at the last 20 years, can we find one tool that we use every day that hasn't been "revolutionized"? Cars -> Electric cars, Appliances -> *Smart* Appliances, Phones -> Smartphones (can we still call those phones?), Lights -> LED, Wires&Cables -> Wireless, Computers that can display text -> Computers that can simulate the whole universe in VR (not really), Money -> Cryptocurrencies, Houses -> Tiny houses, WEB -> WEB2.0 (Graphical) -> WEB3.0 (Apps), Email -> Email. Yup, despite all the changes elsewhere, email hasn't changed.

## i beg to differ
Well, of course, it has changed, there was an *evolution*, a few small incremental updates. Email clients are essentially web browsers, and emails, nowadays, essentially static webpages. Because of this, emails have gotten prettier with time, following the advance of web design. Email clients have gotten smarter, with SPAM filtering, Smart mailboxes, contacts, etc. In fact, for the majority of people, their web browser *is* their email client. That's right, *webmail*. It kinda feels *full-circle-y*.

## except, not
At its core features, modern email is still the same old email. First, let's consider the UI : Most, if not all, email clients display mailboxes on the left sidebar, besides a top to bottom list of email, showing the title and the sender's address. When you write an email, you need more information than in other massaging system : 
- The recipient's address. Some clients have contacts baked-in, some of which you can import/export contacts lists and some simply don't have any contact system.
- A Subject. This actually makes me angry. A Subject needs to be short, and set the tone for the email's content. Also, it can't be too vague. It really doesn't need your name or the date, because the recipient already know that information from metadata. And it cannot be empty. I'm always deeply puzzled as to what to put in a title. Emails should be self-explanatory. Final rant : why put it on top, as one of the first things to fill? When you write a letter, you normally decide on the title at the end, after all your thoughts have been explained.
- The content. Writing an email is still mostly text. Of course you can send links, images, even short videos and on some clients you can go full Microsoft-Word-Clipart-style on it. Even with all that, it's still missing a dynamic WYSIWYG editor. Also 10Mb size limit? smartphone pictures are bigger than that.

## why would i write an email?
Writing email is mostly like sending a *physical* mail. You write something, send it and you never know when the recipient is going to read it.
On most social networks, you can leave messages to others, messages that can be read in the future. BUT, emails aren't social networks, you don't NEED to be trapped inside the email's network in order for it to work. The only thing you need is an address, like a webpage. And it works from everywhere, with everybody.

## peaked or left behind
Will emails get better? I think not. For the moment, it is perfect for what we do with it and will only get displaced when a new paradigm shift will occur.
Yes, it's usage has reduced since social networks are prevailing, but we still can't imagine a world without it.