---
layout: post
title:  "living on the bitcoin standard"
categories: 
    - tinkering
---
## intro
with bitcoin taking more and more space in my life with every new projects, i began to think of living on the bitcoin standard, or making life changes to place bitcoin at the very center. i encourage you to try the same experiment and see how feasible it is in your current situation as well as the different risks and benefits it can bring.

living on the bitcoin standard is like having a short position _against the system_, using your *whole wealth* and in *every transactions*.

lets analyse the idea by reversing the maslow pyramid:

## bitcoin to store my wealth
i'm very lucky to have some wealth accumulated through the years. but how can i protect it from the inflation-theft and keep it's purchasing power for future use?

Since i'm quite young, i can stomach pretty high volatility in the short term if i'm confident there's a high probability of having a great return on a long timeframe.

bitcoin as the perfect case for this, because of its deflationary issuance policy. no other financial product (in my mind) comes close regarding its colossal possible return, of which there's a very high probability given a long enough timeframe.

Following that chain of thought, i transfered all my RRSP savings into a RRSP trading account and bought [BTCC.B](https://www.purposeinvest.com/funds/purpose-bitcoin-etf) shares. In other words, my retirement fund consist of shares of a bitcoin pool. It's not my keys, it's still an IOU-paper-bitcoin, but it allows me to accumulate wealth faster.

Parallel to this, i've been accumulating (lump-sum and DCA) bitcoin in cold storage since 2013. I think of my stack as a savings account.

Even with thoses life-changing moves, i still feel like i'm not doing enough to accumulate and *propagate* bitcoin.

Onto the next phase!

## bitcoin as a pay stub

In the last 10 years, it's gotten very easy to buy bitcoin:

- [Shakepay](https://shakepay.com) allows you to very easily setup reccuring buys and dollar-cost-average.
- [BullBitcoin](https://www.bullbitcoin.com) allows you to buy KYC-Free at every Canada Post location, and other options with minimal KYC
- Hardware wallets like [Coldcard](https://coldcard.com) are getting easier to use

But still, DCA and lump-sum buying of bitcoin requires you to hold dollars in your account. Dollars that I don't want.

So where do these dollars come from? My pay stub.

Let's get paid in bitcoin directly (bi-weekly), and reverse the whole transfer process, selling only once a month, to pay expenses!

First of, the only real option here is with BullBitcoin

Second, how much am i screwing up here? What is the risk profile for the past year? for the past 4 years?
Basicaly, what is the biggest drawback (in $CAD) I can get on a 2-4 weeks bitcoin holding period.
![](https://couteausuis.se/assets/images/Figure_1.png)
This graph shows [the model I created](https://gitlab.com/couteausuisse/couteausuis.se/-/blob/master/assets/python/simulation.py), were I compare 2 options of bitcoin saving:

- in Blue, buying bitcoin at the end of every months, using my budget leftover
- in Red, having a by-weekly pay converted to bitcoin and selling only what I need to cover the monthly expenses at the end of the month.
- in Green, the difference at the end of the month between the two methods

without much surprise, the biggest drawback happened in the bear market, where it costs me more (in bitcoin) to pay my expenses than what i bought for the same $CAD value. During the bull market, I only need to sell a smaller percentage of my bitcoin stack to cover expenses, thus netting a good profit.

![](https://couteausuis.se/assets/images/Figure_2.png)
Third, how much more bitcoin am I accumulating ? This graph shows the same comparison as the previous one, but calculates the accumulated wealth in btc instead of in $CAD. Same conslusion, in the bear market it's very costly but during the bull phase I can accumulate faster.

The ideal scenario, it seems, would be to get paid in bitcoin during the bull run and revert back to monthly DCA during the bear market.


## bitcoin to pay bills

Nice, now i'm getting paid in bitcoin!
But wait, I still have bills to pay:

- appartement/mortgage
- inssurance
- electricity
- internet
- credit card

Let's see if some of those utilities want to take straight bitcoin?

Nope. I'll work on that

In the meantime, i'll use [Bylls](https://bylls.com) from BullBitcoin to pay my bills using bitcoin
Now, all the leftover bitcoin from a month will go straight to my cold storage 

## bitcoin as a currency

Let's push the idea further and try to live by spending only bitcoin.
Now this might seem counter-intuitive, since spending dollars and accumulating bitcoin is the perfect short position, but increasing bitcoin's usage and merchants that accept it is a net positive.

I'll call every merchant from the pasts months and ask if they accept bitcoin. If not, i'll try to orange-pill them.