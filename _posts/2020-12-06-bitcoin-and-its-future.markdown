---
layout: post
title:  "bitcoin and the future"
categories: 
    - tinkering
    - asteroidlabs
---
## what is bitcoin
bitcoin is a form of digital currency, where every transaction, once confirmed in the blockchain, are irreversible. Its ease of use, high security, merchants and platforms support and high market capitalization makes it the most popular asset to transact digitally. It is not anonymous by any means, however, the identity of the user behind the address can be unknown.

There is a fixed supply of 21M bitcoins, and countless lost during the early days of the currency. The coins are issued to miners for every block that gets confirmed on the blockchain. The issuance policy follows a rule dictating that roughly every 4 years, the number of bitcoins issued to miners is cut in half (halving).

while bitcoin is a software program, all the actors have incentives to stick to the same working code, increasing the confidence in the network. Code changes need to be approved by different crowds each with different usages & incentives (developers, nodes operators, pools, miners, users), making it very difficult to rebalance the power to one group.


## digital scarity / Stock-to-Flow
more and more merchants are accepting bitcoin every day, but most of its volume is based on price speculation, as it's true potential makes it critically undervalued.

> "As a thought experiment, imagine there was a base metal as scarce as gold but with the following properties: boring grey in colour, not a good conductor of electricity, not particularly strong [..], not useful for any practical or ornamental purpose .. and one special, magical property: can be transported over a communication channel" — Nakamoto [2]

This magical property has tremendous value, and multiple models are trying to calculate bitcoin's true price. The S2F/S2FX model is the one that makes most sense to me.

Nick Szabo has an interesting take on 'scarity' and defines it has 'unforgeable costliness', or since it cost so much money to create a real one, it's creation cannot be easily faked. This can be easily applied to bitcoin, as it cost a lot of electricity to produce new bitcoin (PoW), producing bitcoin can't be easily faked.

In terms of stock-to-flow ratios, often used for scarce metals and consumable commodities, scarity can be explained as follows: 

> "For any consumable commodity [..] doubling of output will dwarf any existing stockpiles, bringing the price crashing down and hurting the holders. For gold, a price spike that causes a doubling of annual production will be insignificant, increasing stockpiles by 3% rather than 1.5%."
> 
> "It is this consistently low rate of supply of gold that is the fundamental reason it has maintained its monetary role throughout human history."
> 
> "The high stock-to-flow ratio of gold makes it the commodity with the lowest price elasticity of supply."

> "The existing stockpiles of Bitcoin in 2017 were around 25 times larger than the new coins produced in 2017. This is still less than half of the ratio for gold, but around the year 2022, Bitcoin's stock-to-flow ratio will overtake that of gold" — Ammous[5]

In the stock-to-flow model, stock represents the size of the existing reserves, whereas flow is the yearly production. Gold as an SF of 62, meaning that it takes 62 years of production to get as much gold as the current gold reserves.


Since it has been [proven](https://medium.com/@100trillionUSD/modeling-bitcoins-value-with-scarcity-91fa0fc03e25) that scarity, but mainly stock-to-flow values is directly linked to price movements, we can safely state that a rise in scarity will result in a rise in price.

Furthermore, since bitcoin [follows](https://medium.com/@100trillionUSD/bitcoin-stock-to-flow-cross-asset-model-50d260feed12) the same scarity law as precious metals at every halving, its price should rise tremendously about every 4 years (with a few months of lag).

Follow the stock-to-flow model applied to bitcoin price movements [here](https://digitalik.net/btc).

## institutions buy-in
in the 11 years since its inception, a lot of big players have watched on the sidelines to better understand how bitcoin behaves. Every week, big institutions (Grayscale, Microstrategy, Fidelity), VC funds and billionaires (Paul Tudor Jones,) are buying huge quantities of bitcoin. Cumulatively, they buy much more than the daily production.

[more information here](https://www.newsbtc.com/news/bitcoin/institutions-pouring-millions-bitcoin/)

This is not yet priced-in.
as Hemingway said, "Gradualy, then suddenly"

## the inevitable future

What the multiple market crash of fiat has proven to us is that the current monetary supply and financial system is deeply flawed and not fixable.

Since 2008, most governments have adopted an infinite monetary supply, or Quantitative Easing, leading to an infinite inflation.
20% of all US dollars were printed in 2020 alone. This directly translates to a diminishing value of every US dollars by 20%.

All around the world, debts are growing at never seen before rates, while governments apply the same monetary policies and have no intentions of reducing debts.

If you look at the purchasing power of the US dollar since 1971, 1000$ represents a purchasing power of only 150$ in 2020.

With bitcoin and its inherent scarity, 1 bitcoin will always be equal to 1 bitcoin in 50 years.

Bitcoin is a hedge against the diminishing value of fiat currencies and a much better store of value for your wealth in this uncertain future.


## the cost of a sell
Selling an asset early will appear like a loss in the future.

Let's look at our friend Steve, who's in this situation:

* Bought 1 bitcoin at 10K$
* bitcoin is now trading at 20K$ (2x)
* He firmly believes that a bitcoin can reach 200K$ (20x) in the next few years

If Steve sells 0.25btc now, he'll make a 2.5K$ profit (2x ROI). 
In a few years, once a bitcoin is worth 200K$, his 0.25btc he sold would have been worth 50K$. Selling early will result in a loss of 47.5K$.

## counter arguments

bitcoin has a few counter arguments, and i'll to explain the most valid of them here.

### bitcoin has no intrinsic value
While it's true that it's hard to calculate its intrinsic value, it does not mean that there is none. bitcoin is a completely new asset/tool/currency/, etc., and the humanity are only beginning to uncover its potential. Almost all paradigm shift had no intrinsic values at their launch: the car, internet, social networks.

### bitcoin is unstable and based on speculation
In the past few years, every market was illiquid, resulting in a lot of price fluctuations. The fact that not a lot of people were trading it meant that price could move by a lot in a few minutes. Nowadays, we have big institutions, OTC trading, billions in liquidity. The fact is, price has been pretty stable in the past 2 years. Transaction time was also unaffected.

### bitcoin uses (waste) too much energy
Has I explained earlier, each bitcoin is a proof a work done. That work needed tremendous amount of energy to be executed. It is also the key concept of bitcoin, its value proposal and what makes it irreversible. In other words, this can't be fixed. However, technological improvements can lead to improved efficiency of the miners. Furthermore, since miners are paying for this electricity, they are in search of the cheapest electricity source, which happens to be Solar, the cleanest of them all. Even now, with a volume and transaction value per second never seen before, the whole bitcoin network uses less than the banking system it will replace.