---
layout: post
title:  "archive: lejacobroy.com"
categories: 
    - archives
---

in 2011, when i was only 15 years old, i decided to build myself a website to showcase my photographs. while working on it, i began adding modules and expending features: 
- handmade front-end design
- front & back ends build from nothing, in PHP5 and MySQL
- when uploading a photograph, the back-end will extract it's metadata and store it in the DB
- fully-featured shopping cart to buy pictures through PayPal (PP integration no longer working)
- blogging section using WordPress (no longer functional)
- showcase section with other art projects
- users would buy pictures and received an encrypted link by email, allowing them to download a server-generated zip file containing the picture and license.

it then began clear that programming was a bigger passion than photography!
a year later, when i backed up the site, i forgot to choose a latin encoding, resulting in weird glyphs on the restored website

please, dont judge

[lejacobroy.com (moved to http://lejacobroy.000webhostapp.com/)](http://lejacobroy.000webhostapp.com/)