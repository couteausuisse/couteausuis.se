---
layout: post
title: "migrate Apple Photos to Immich"
categories: 
    - tinkering
---
## intro
tired of having to pay for cloud storage (basically forever), i began the journey to migrate my Apple Photos Library to a self-hosted Immich server. here’s the steps i took.

## exporting for Photos.app
the first task is to download all the originals using the macOS Photos.app. Simply check the ‘Download Originals to the Mac’ in the iCloud preferences.
![](https://couteausuis.se/assets/images/icloud_photos.png)
Let the app download every asset, you can watch the progress in you Library.

Then, we’ll be using [osxphotos](https://github.com/RhetTbull/osxphotos) to do the actual export, as the Export feature in Photos.app is too restrictive.
*pro tip: do the initial upload over LAN, it will be much faster and you will not hit upload size restrictions.*
`osxphotos` has lots and lots of options and it can seem very daunding at first, but here’s a simple command to do the job:

`osxphotos export yourFolder --skip-original-if-edited --sidecar XMP --touch-file --directory “{folder_album}” --download-missing`

you can change the options to your liking, here’s the breakdown:
- --skip-original-if-edited : will export the edited files only, and the originals if there’s no edited version.
- --sidecar XMP : generate a corresponding XMP sidecar file for every assets, that contains more metadata (faces, edits, etc).
- --touch-file : retains the original file date instead of the exported date.
- --directory “{folder_album}” : create folders based on the albums (they will be used to import to Immich after).
- --download-missing : even tho we downloaded originals in the Photos app, some assets might still need downloading.

Once the files are exported, we’ll upload them to the Immich instance.

## upload to Immich
once you have your [immich](https://immich.app) server running, bulk uploading is pretty straightforward.
following the docs for the immich-cli tool, i’m using this command to upload the files:

`immich upload --key YourAPIKey --server yourLANIP --recursive yourFolder --album`
- --recursive instruct the tool to upload the assets from every subfolders.
- --album will create immich albums from the parent folder of the assets.
don’t worry about duplicates as immich makes a good job to de-dupe the files and the albums.

## next steps
you can now delete all assets in your iCloud Photos Library and free up costly cloud space. the Immich ios app does a great job to sync new assets to the server.