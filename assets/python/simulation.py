import matplotlib.pyplot as plt
from matplotlib import rc
rc('mathtext', default='regular')
import json
import datetime as dt
import pandas as pd

def calculate_accumulated_wealth(prices):
    buying_amount = 1822
    selling_amount = 3438
    wealth_btc = 0.000
    total_wealth = 0.000
    total_wealth_btc = 0.000
    compare_wealth = 0.000
    compare_wealth_btc = 0.000
    diff_cad = 0.000
    diff_btc = 0.000
    accumulated_wealth = []
    day_counter = 1

    for day in prices:
        #print(day["date"], day_counter, day["price"], total_wealth, wealth)
        if day_counter % 14 == 0 :
            wealth_btc += buying_amount/day["price"]
            #print(day, " Week:", wealth)

        if day_counter != 0 and day_counter % 28 == 0:
            wealth_btc -= selling_amount/day["price"]
            #print(day["date"], " Month:", wealth)
            #compare_wealth += ((buying_amount*2)-selling_amount)
            compare_wealth_btc += ((buying_amount*2)-selling_amount)/day["price"]
            diff_btc = wealth_btc-compare_wealth_btc
            diff_cad = diff_btc*day["price"]

        day_counter += 1
        total_wealth_btc = wealth_btc
        total_wealth = total_wealth_btc*day["price"]
        compare_wealth = compare_wealth_btc*day["price"]
        accumulated_wealth.append( 
            {'date':day["date"],
             'price': day["price"],
             'total_wealth': total_wealth,
             'total_wealth_btc': total_wealth_btc,
             'compare_wealth': compare_wealth,
             'compare_wealth_btc': compare_wealth_btc,
             'diff_btc': diff_btc,
             'diff_cad': diff_cad
            })
        #wealth_btc = 0.0000

    return accumulated_wealth


def plot_cad(data, color):
    # Generate graph
    #plt.axis([-1000,10000])
    plt.figure(1)
    plt.plot(data, color)
    plt.xlabel("Date")
    plt.ylabel("Accumulated Wealth ($cad)")
    plt.title('Accumulated Wealth')
    plt.grid(True)
    plt.gcf().autofmt_xdate()

def plot_btc(data, color):
    # Generate graph
    plt.figure(2)
    #plt.axis([-1,1])
    plt.plot(data, color)
    plt.xlabel("Date")
    plt.ylabel("Accumulated Wealth (btc)")
    plt.title('Accumulated Wealth')
    plt.grid(True)

# Opening JSON file
with open('data_4y.json', 'r', encoding='utf-8') as json_file:
    data = json.load(json_file)

# Calculate accumulated wealth
    accumulated_wealth = calculate_accumulated_wealth(data)

    # Generate table
    print("Day\t\tAccumulated Wealth")
    print("---------------------------")
    for dat in accumulated_wealth:
        print(dat)

    print("-- accumulated wealth (Btc) from pay stub : ",accumulated_wealth[len(accumulated_wealth)-1]["total_wealth_btc"], " worth : ", accumulated_wealth[len(accumulated_wealth)-1]["total_wealth"], "$CAD")
    print("-- compared wealth (Btc) from end of month buying : ",accumulated_wealth[len(accumulated_wealth)-1]["compare_wealth_btc"], " worth : ", accumulated_wealth[len(accumulated_wealth)-1]["compare_wealth"], "$CAD")

    plot_cad([[d['total_wealth']] for d in accumulated_wealth], 'r')
    plot_cad([[d['compare_wealth']] for d in accumulated_wealth], 'b')
    plot_cad([[d['diff_cad']] for d in accumulated_wealth], 'g')
    plot_btc([[d['total_wealth_btc']] for d in accumulated_wealth], 'r')
    plot_btc([[d['compare_wealth_btc']] for d in accumulated_wealth], 'b')
    plot_btc([[d['diff_btc']] for d in accumulated_wealth], 'g')
    plt.show()

