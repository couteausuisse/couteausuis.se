import requests
#import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import time
import json

# CoinGecko API endpoint for historical price data
API_URL = "https://api.coingecko.com/api/v3/coins/bitcoin/history"

with open('data_4y.json', 'w', encoding='utf-8') as f:
    today = datetime.now()
    one_year_ago = today - timedelta(days=(365*4))
    date_format = "%d-%m-%Y"

    prices = []
    current_date = one_year_ago

    while current_date <= today:
        date_str = current_date.strftime(date_format)
        time.sleep(5)
        response = requests.get(API_URL, params={"date": date_str, "localization": "false"})
        if response.status_code == 200:
            data = response.json()
            if data:
                current_date += timedelta(days=1)
                ##print(data)
                ##timestamp = data[0]["timestamp"] // 1000
                price = data["market_data"]["current_price"]["cad"]
                data = {'date': date_str, 'price': price}
                prices.append(data)
                print(data)
        else:
            time.sleep(20)
    json.dump(prices, f, ensure_ascii=False, indent=4)
    #return prices

# Retrieve historical price data
#prices = get_historical_prices()

#    myfile.write('\n'.join(prices))
# Generate graph
"""     plt.plot(prices)
    plt.xlabel("Day")
    plt.ylabel("Bitcoin price ($ CAD)")
    plt.title("Bticoin price over a Year")
    plt.grid(True)
    plt.show() """
