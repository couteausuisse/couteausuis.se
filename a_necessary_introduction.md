---
layout: page
title: a necessary introduction
---

<p class="message">
 Web Developer 💡. Photographer 📸. Wind Turbine Revolutionary 🌪. Cryptocurrency Astronaut 👩🏼‍🚀.
</p>

## Hey there!
I'm Jacob, from Québec. Where I come from, we say *Un vrai couteau suisse* (*A true Swiss knife*), to say that a person is variously skilled and surprises you with unexplained or uncommon knowledge. A bunch of friends started to call me *couteau suisse*, hence the name of the blog.

## History
Since I was a child, I was always interested in electronics. My grandfather, being an electronics teacher, helped me a lot to learn the basics of circuitry and electricity rules. During my teenage years, I discovered web development, and have since built multiple website projects. Before the end of High School, I had built (from scratch) an e-commerce portfolio to sell digital photographs using a custom CMS and PayPal integration. I also built and maintained a Cydia Repository for jailbroken iOS devices, iCauseFX. After that, I studied Computer Sciences as well as Electronics & Telecommunications, where I learned the *logical and academic* programing, CAD Design, Networking and Wireless protocols.


## jacobroy.quebec
Wanting a clean slate to experiment minimal UI and new web paradigms, I set sail to rebuild my portfolio website, this time using Jekyll, GitHub Pages, Stripe and Snipcart. In its fifth year, the project has gone through many iterations. In its current form, Stripe and Snipcart has been put aside. The whole UI is very minimal and soft to the eyes, putting the emphasis on the photographs and the various contexts and thoughts added. Take a look at [jacobroy.quebec](https://jacobroy.quebec), also available on [GitHub](https://github.com/lejacobroy/lejacobroy.github.io).

## archives
A humble place where my old or unused projects can lay back and enjoy the sun. Most of them should still be working.

## tinkering
Multiple unfinished or proof-of-concept projects that helps me get the job done.

## couteausuis.se
This blog is lazyly built from Jekyll and uses the [Lanyon](http://lanyon.getpoole.com) theme with some modifications.